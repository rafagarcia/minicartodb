# Minicartodb.js v1.0.0

## What is miniCartoDB?

miniCARTODB is a library writen by @therealpablors for a CARTO frontend test 2017.

## What can I do with miniCartoDB?

The main idea is to write a little javascript library that read a specific config file and:

* Renders the map using one of the available mapping libraries (leaflet and openlayers)
* Exposes an API to:
	* Show / hide any of the map layers.
	* Update the 'sql' attribute of CartoDB layers that are rendered by Maps API.

And using browserify for fun.

## How to use it

Add ./build/minicartodb.js.min to your site:

    <script src="./build/minicarto.min.js"></script>

and create the map with a valid json config file. For example:

    <script>
        function main() {
          var vizjson = {
            "center": "[52.5897007687178, 52.734375]",
            "zoom": 4,
            "maps_api_config": {
              "user_name": "documentation",
              "maps_api_template": "http://{user}.cartodb.com:80"
            },
            "map_provider": 'leaflet',
            "layers": [
              {
                "type": "tiled",
                "options": {
                  "urlTemplate": "http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png",
                  "minZoom": "0",
                  "maxZoom": "18",
                  "attribution": "&copy; <a href=\"http://www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors"
                }
              },
              {
                "type": "CartoDB",
                "options": {
                  "sql": "select * from european_countries_e",
                  "cartocss": "/** choropleth visualization */\n\n#european_countries_e{\n  polygon-fill: #FFFFB2;\n  polygon-opacity: 0.8;\n  line-color: #FFF;\n  line-width: 1;\n  line-opacity: 0.5;\n}\n#european_countries_e [ area <= 1638094] {\n   polygon-fill: #B10026;\n}\n#european_countries_e [ area <= 55010] {\n   polygon-fill: #E31A1C;\n}\n#european_countries_e [ area <= 34895] {\n   polygon-fill: #FC4E2A;\n}\n#european_countries_e [ area <= 12890] {\n   polygon-fill: #FD8D3C;\n}\n#european_countries_e [ area <= 10025] {\n   polygon-fill: #FEB24C;\n}\n#european_countries_e [ area <= 9150] {\n   polygon-fill: #FED976;\n}\n#european_countries_e [ area <= 5592] {\n   polygon-fill: #FFFFB2;\n}",
                  "cartocss_version": "2.1.1"
                }
              },
              {
                "options": {
                  "urlTemplate": "http://{s}.basemaps.cartocdn.com/light_only_labels/{z}/{x}/{y}.png",
                  "minZoom": "0",
                  "maxZoom": "18",
                  "attribution": "&copy; <a href=\"http://www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors"
                },
                "type": "tiled"
              }
            ],
          };

          minicarto.createVis('map', vizjson, {groupLayers: true});
        }
        window.onload = main;
      </script>

Now you have minicarto library instantiate into your site and you can:

- Create any map if you want:

    minicarto.createVis({htmlSelector}, {mapConfigJson});


**You can set "mapProvider" property in mapConfiJson if you want to use a specific map provider. At this moment the library only have two providers: leaflet and openlayers. If you don't set any map provider the library will use leaflet.*

 - group layers for show or hide them later:

    minicarto.createVis('map', minicarto._vizjson, {groupLayer: true});

 - Show/hide any of the layers:

    minicarto.disableLayer(1);
    minicarto.enableLayer(1);

 - Change sql query of the CartoDB layer:

    minicarto.setSQL('select * from european_countries_e LIMIT 10');

## How to build

Build minicartodb.js library:

 1. Install [node.js](https://nodejs.org), from 6.10 LTS version
 2. npm install
 3. npm run gulp:build to create minicartodb.min.js
 4. You have your library for mapping!

## How to test

minicartodb have a lot of tests but if you want to do your best this is the way to test:

1. npm run test or npm test

minicartodb uses: karma, jasmine and proxyquireify

## The Statement

###Engine Test


####Task
Create a mini version of CartoDB.js from scratch. 

####Context
The Engine team is working on the next version of CartoDB.js. We’ll be rewriting the API, supporting many new requirements from BUILDER and clients, and incorporating a new backend. This will involve some changes to the library.

CartoDB.js is a library that abstracts several libraries (Leaflet, Tangram, Google Maps) to offer a unified API that developers can use to render and customise maps. On top of that, the new version of the library will provide functionality to manage widgets, analysis, etc.

The library integrates with CARTO’s Maps API, which acts as the backend that provides tiles, interactivity grids, feature attributes, etc. One of the main challenges of the library is making all these components work / communicate nicely together.

Sounds interesting to you? We would love to see how you would tackle these kind of problems by solving this little test.

####Goals
Your mission is to write a little JS library (a mini version of CartoDB.js) that reads a config file describing a map and:
- Renders the map using one of the available mapping libraries (Leaflet, OpenLayers, Google Maps, d3.js… you choose!).
- Exposes an API to:
- - Show / hide any of the map layers.
- - Update the “sql” attribute of CartoDB layers that are rendered by Maps API.

This is quite a lot of work, but we think it’s interesting! There’s no time limit for this test. We know you probably have a busy life, so please, do as much as you possible can! And please, don’t let the scope of this test push you back. If you can only finish goal #1, it’s totally okay!

####What you SHOULD do
- Review our job offer and apply here.
- Think about the architecture of your library, try to split your code into components / modules / files / folders.
- Store your code in a github / gitlab / bitbucket repo.
- Prepare an HTML file that uses your library to render the map and allows us to test the library from the browser’s console. Something similar to this.
Include a README file describing the library and explaining how to “install” and use it.

####What you CAN do
- Use Backbone.js (CartoDB.js uses it) or go with vanilla JS.
- Use any other little JS library that might help you accomplish some basic things (eg: underscore, jQuery, etc.)
- Use any bundling tool if you need to.
- Use ES5/6/7.

####What we will be looking for
- A nice architecture with nicely decoupled components. We’ll be asking ourselves questions like: “How easy would it be to switch the mapping library?”.
- Code that we can easily understand and makes sense to us.
- A developer friendly API.

####Test description
Goal #1 - Render the map
Your library should be able to read the following config file:

    {
      "center":"[52.5897007687178, 52.734375]",
      "zoom":4,
      "maps_api_config": {
        "user_name": "documentation",
        "maps_api_template": "http://{user}.cartodb.com:80"
      },
      "layers":[
        {
          "type":"tiled",
          "options":{     "urlTemplate":"http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png",
            "minZoom":"0",
            "maxZoom":"18",
            "attribution":"&copy; <a href=\"http://www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors"
          }
        },
        {
          "type":"CartoDB",
          "options":{
            "sql":"select * from european_countries_e",
            "cartocss":"/** choropleth visualization */\n\n#european_countries_e{\n  polygon-fill: #FFFFB2;\n  polygon-opacity: 0.8;\n  line-color: #FFF;\n  line-width: 1;\n  line-opacity: 0.5;\n}\n#european_countries_e [ area <= 1638094] {\n   polygon-fill: #B10026;\n}\n#european_countries_e [ area <= 55010] {\n   polygon-fill: #E31A1C;\n}\n#european_countries_e [ area <= 34895] {\n   polygon-fill: #FC4E2A;\n}\n#european_countries_e [ area <= 12890] {\n   polygon-fill: #FD8D3C;\n}\n#european_countries_e [ area <= 10025] {\n   polygon-fill: #FEB24C;\n}\n#european_countries_e [ area <= 9150] {\n   polygon-fill: #FED976;\n}\n#european_countries_e [ area <= 5592] {\n   polygon-fill: #FFFFB2;\n}",
            "cartocss_version":"2.1.1"
          }
        },
        {
          "options": {
            "urlTemplate": "http://{s}.basemaps.cartocdn.com/light_only_labels/{z}/{x}/{y}.png",
            "minZoom": "0",
            "maxZoom": "18",
            "attribution": "&copy; <a href=\"http://www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors"
          },
          "type": "tiled"
        }
      ],
    }

As you can see, the config file contains:
- Some initial config for the map (center & zoom).
- Maps API configuration details.
- 3 layers:
- - A tiled layer with options for the basemap.
- - A layer with some SQL and CartoCSS.	
- - A tiled layer with options for the labels.

Tiled layers contain XYZ URL templates so they should be pretty straight forward to render(eg: using a L.TileLayer if you go with Leaflet).

You will also be able to render the CartoDB layer with a plain old tiled layer, but… where’s the URL template to fetch those tiles? Don’t worry, we’ll help you this!

The Maps API is the component that renders tiles for CARTO maps. Before you can fetch tiles for a map, your library will need to talk to Maps API so that it has some info about the tiles that you’re going to request. Here’re the steps that you’ll need to follow to generate a URL template for the CartoDB layer:

1. Make a POST request to:
http://documentation.cartodb.com/api/v1/map

You should be able to easily generate this ^^ URL from the maps_api_config in the config file.
You should also generate the payload (the MapConfig) for this request. All of the information you will need is in the config file as well. It should like this:

    {
      "layers": [
        {
          "type": "http",
          "options": {
            "urlTemplate": "http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png"
          }
        },
        {
          "type": "mapnik",
          "options": {
            "cartocss": "/** choropleth visualization */\n\n#european_countries_e{\n  polygon-fill: #FFFFB2;\n  polygon-opacity: 0.8;\n  line-color: #FFF;\n  line-width: 1;\n  line-opacity: 0.5;\n}\n#european_countries_e [ area <= 1638094] {\n   polygon-fill: #B10026;\n}\n#european_countries_e [ area <= 55010] {\n   polygon-fill: #E31A1C;\n}\n#european_countries_e [ area <= 34895] {\n   polygon-fill: #FC4E2A;\n}\n#european_countries_e [ area <= 12890] {\n   polygon-fill: #FD8D3C;\n}\n#european_countries_e [ area <= 10025] {\n   polygon-fill: #FEB24C;\n}\n#european_countries_e [ area <= 9150] {\n   polygon-fill: #FED976;\n}\n#european_countries_e [ area <= 5592] {\n   polygon-fill: #FFFFB2;\n}",
            "cartocss_version": "2.1.1",
            "sql": "select * from european_countries_e",
            "interactivity": [
              "cartodb_id"
            ]
          }
        },
        {
          "type": "http",
          "options": {
            "urlTemplate": "http://{s}.basemaps.cartocdn.com/light_only_labels/{z}/{x}/{y}.png"
          }
        }
      ]
    }

2. Generate the XYZ URL template to fetch tiles.
This previous request should return something like:

    {
      "layergroupid": "28186517f686d343f8e69342c5d74fe9:0",
      "metadata": {
        "layers": [ … ]
      }, ...
    }

As long as you get a “layergroupid” and you can see some metadata for each of the layers, you are in a great position to now generate the XYZ URL template to fetch tiles for this map. Here’s what the template should look like:

![enter image description here](https://lh3.googleusercontent.com/-lzTS0CRjy3Y/WYBJKyjxr3I/AAAAAAAAAyQ/DA6i-dpArRkfHd_LBdpxcshO-mpTOUM-ACLcBGAs/s0/Captura+de+pantalla+2017-08-01+a+las+11.22.15.png "url example")

3. Fetch tiles
As mentioned, once you have your URL template, you will be able to use a tiled layer that will fetch tiles and render them in the map. Your map should look like this:

![enter image description here](https://lh3.googleusercontent.com/-LSN5FK_14Fg/WYBJCRo8bDI/AAAAAAAAAyI/AjBKHioXHh0sZowvVVsVBSCG6dlE3JDJwCLcBGAs/s0/Captura+de+pantalla+2017-08-01+a+las+11.24.28.png "carto europe map")

#### Goal #2 - Create a little API for the library
This API should allow users to:
1. Show / hide any of the layers (eg: layer.hide())
2. Change the “sql” of the CartoDB layer (eg: layer.setSQL("select * from european_countries_e LIMIT 10"))
When users change the SQL of the CartoDB layer, your library will have to trigger a new request to the Maps API and generate a new url template to fetch new tiles for the CartoDB layer again. After changing the SQL, your map should no longer show all countries.

![enter image description here](https://lh3.googleusercontent.com/-kvu7F7OQ330/WYBJeaJrlBI/AAAAAAAAAyY/gD3ZZBULcK8TEsw3egnBeXksatC5pn-NgCLcBGAs/s0/Captura+de+pantalla+2017-08-01+a+las+11.26.53.png "carto europe map with less countries")

---
We can’t wait to see what you come up with!
Thanks for you time!
THE END.´

## Requirements

miniCartoDB works in any modern browser, but if you want more info:

![Chrome](https://cdnjs.cloudflare.com/ajax/libs/browser-logos/39.3.0/archive/chrome_12-48/chrome_12-48_48x48.png) | ![Firefox](https://cdnjs.cloudflare.com/ajax/libs/browser-logos/39.3.0/archive/firefox_1.5-3/firefox_1.5-3_48x48.png) | ![IE](https://cdnjs.cloudflare.com/ajax/libs/browser-logos/39.3.0/edge-tile/edge-tile_48x48.png) | ![Opera](https://cdnjs.cloudflare.com/ajax/libs/browser-logos/39.3.0/opera/opera_48x48.png) | ![Safari](https://cdnjs.cloudflare.com/ajax/libs/browser-logos/39.3.0/safari/safari_48x48.png)
--- | --- | --- | --- | --- |
31+ ✔ | 38+ ✔ | 11+ ✔ | 31+ ✔ | 8+ ✔ |
