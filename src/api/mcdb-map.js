//  minicartodb.js 1.0.0
//  mcdb map
//  https://therealpablors@bitbucket.org/therealpablors/minicartodb.git
//  (c) 2017 Pablo Rodríguez, @therealpablors
//  Carto Test Frontend developer 2017
//  minicarto may be freely distributed under the MIT license.

'use strict';

/**
 * Dependencies
 */
var mcdbJSonParser = require('../utils/mcdb-parser');
var _ = require('underscore');

var mcdbMap = {
  _layergroupid: 0,
  _xyzTemplateURL: 'http://ashbu.cartocdn.com/{username}/api/v1/map/{layergroupid}/{index}/{z}/{x}/{y}.png'
};

var createURLSxyz = function(urlTemplate, data) {
  _.templateSettings = {
    interpolate: /\{(.+?)\}/g
  };
  data.x = '{x}';
  data.y = '{y}';
  data.z = '{z}';
  var template = _.template(urlTemplate);
  return template(data);
};

mcdbMap.create = function(selector, mapInfo, provider, map) {
  var keys = Object.keys(map || {});
  switch(provider) {
  case 'leaflet':
    var L = window.L;
    if (keys.length > 0) {
      map.remove();
    }
    return L.map(selector).setView(mapInfo.center, mapInfo.zoom);
    break;
  case 'openlayers':
    var ol = window.ol;
    if (keys.length > 0) {
      map.setTarget(null);
      map = null;
    }
    return new ol.Map({
      target: selector,
      layers: [
        new ol.layer.Tile({
          source: new ol.source.OSM()
        })
      ],
      view: new ol.View({
        center: ol.proj.fromLonLat(mapInfo.center),
        zoom: mapInfo.zoom
      })
    });
    break;
  default:
    break;
  }
};

mcdbMap.createLayers = function(data, config, provider) {
  var self = this;
  self._layergroupid = data.layergroupid;
  switch(provider) {
  case 'leaflet':
    var L = window.L;
    return L.tileLayer(self._xyzTemplateURL, {
      username: config['user_name'],
      layergroupid: self._layergroupid,
      maxZoom: config.maxZoom || 18,
      minZoom: config.minZoom || 1,
      index: _.range(data.metadata.layers.length)
    });
    break;
  case 'openlayers':
    var ol = window.ol;
    var xyzURL = createURLSxyz(self._xyzTemplateURL, {
      username: config['user_name'],
      layergroupid: self._layergroupid,
      maxZoom: config.maxZoom || 18,
      minZoom: config.minZoom || 1,
      index: _.range(data.metadata.layers.length)
    });
    return new ol.layer.Tile({
      source: new ol.source.XYZ({
        url: xyzURL
      })
    });
    break;
  default:
    break;
  }
};

mcdbMap.createGroupLayers = function(data, config, provider) {
  var self = this;
  var layers = _.range(data.metadata.layers.length);
  var layersGroup = {};
  self._layergroupid = data.layergroupid;
  var info = {
    username: config['user_name'],
    layergroupid: self._layergroupid,
    maxZoom: config.maxZoom || 18,
    minZoom: config.minZoom || 1
  };
  switch(provider) {
  case 'leaflet':
    var L = window.L;
    _.each(layers, function(layer, index) {
      info.index = index;
      layersGroup['layer ' + index] = L.tileLayer(self._xyzTemplateURL, info);
    });
    var baseLayer = this.createLayers(data, config, provider);
    return {
      control: L.control.layers(layersGroup),
      layersGroup: layersGroup,
      baseLayer: baseLayer
    };
    break;
  case 'openlayers':
    var ol = window.ol;
    _.each(layers, function(layer, index) {
      info.index = index;
      var xyzURL = createURLSxyz(self._xyzTemplateURL, info);
      layersGroup['layer' + index] = new ol.layer.Tile({
        source: new ol.source.XYZ({
          url: xyzURL
        })
      });
    });
    var baseLayer = this.createLayers(data, config, provider);
    return {
      layersGroup: layersGroup,
      baseLayer: baseLayer
    };
    break;
  default:
    break;
  }
};

mcdbMap.setOpacityToLayer = function(layerIndex, opacity, layers, provider) {
  var keys = Object.keys(layers);
  switch(provider) {
  case 'leaflet':
    return layers[keys[layerIndex]].setOpacity(opacity);
    break;
  case 'openlayers':
    return layers[keys[layerIndex]].setOpacity(opacity);
    break;
  default:
    break;
  }
};

module.exports = mcdbMap;
