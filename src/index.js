//  minicartodb.js 1.0.0
//  bitbucket repo: https://therealpablors@bitbucket.org/therealpablors/minicartodb.git
//  (c) 2017 Pablo Rodríguez, @therealpablors
//  Carto Test Frontend developer 2017
//  minicarto may be freely distributed under the MIT license.

var Mcdb = require('./core/mcdb');
var emitter = require('./utils/mcdb-events');
var emitterMessages = emitter.eventBus._messages;

var minicarto = new Mcdb();

//wait for event
emitter.eventBus.on(emitterMessages.getCartoData, function(data) {
  minicarto.createLayers(data);
});

// export minicarto
window.minicarto = minicarto;
