//  minicartodb.js 1.0.0
//  mcdb core
//  https://therealpablors@bitbucket.org/therealpablors/minicartodb.git
//  (c) 2017 Pablo Rodríguez, @therealpablors
//  Carto Test Frontend developer 2017
//  minicarto may be freely distributed under the MIT license.

'use strict';

/**
 * Dependencias
*/
var mcdbMap = require('../api/mcdb-map');
var McdbCall = require('../utils/mcdb-call');
var mcdbJSonParser = require('../utils/mcdb-parser');
var emitter = require('../utils/mcdb-events');
var emitterMessages = emitter.eventBus._messages;

var _ = require('underscore');

/**
 * defaults properties
 */
var defaults = {
  mapProvider: 'leaflet',
  zoom: 1,
  carto_api: {
    protocol: 'https',
    domain: 'carto.com',
    url: 'api/v1/map'
  }
};

var dependencies = [
  'https://code.jquery.com/jquery-2.2.4.min.js'
];

var mapsLibraries = {
  'leaflet': {
    'js': ['https://unpkg.com/leaflet@1.1.0/dist/leaflet.js'],
    'css': ['https://unpkg.com/leaflet@1.1.0/dist/leaflet.css']
  },
  'openlayers': {
    'js': ['https://openlayers.org/en/v4.2.0/build/ol.js'],
    'css': ['https://openlayers.org/en/v4.2.0/css/ol.css']
  }
};

var validProviders = _.keys(mapsLibraries);

function loadResource(dependencies, type, customMessage) {
  var dependenciesMaxIndex = dependencies.length - 1;
  _.each(dependencies, function(url, index) {
    if (type === 'script') {
      var script = document.createElement('script');
      script.src = url;
      if (dependenciesMaxIndex === index) {
        script.onload = function() {
          if (customMessage) {
            emitter.eventBus.sendEvent(customMessage);
          }
        };
      }
      document.body.appendChild(script);
    } else if (type == 'css') {
      var link = document.createElement('link');
      link.type = 'text/css';
      link.rel = 'stylesheet';
      link.media = 'screen,print';
      link.href = url;
      document.head.appendChild(link);
    }
  });
}

/**
 * Creates a new minicarto object
 */
function MiniCarto() {
  this.VERSION = '1.0.0',
  this._vizjson = {};
  this._htmlSelector = '';
  this._nativeMap = {};
  this._dependencies = dependencies;
  this._mapProviderError = 'Your map provider is not implemented yet. Please select one from: ' + validProviders;
  this._options = _.extend({}, defaults);
  loadResource(this._dependencies, 'script');

  return this;
}


MiniCarto.prototype.createMap = function(selector, vizjson) {
  var self = this;
  var mapProvidedLoadedMessage = emitterMessages.mapProviderLoaded;
  self._options.mapInfo = mcdbJSonParser.getMapInfo(vizjson);

  if (self._options.mapInfo.mapProvider === 'default') {
    self._options.mapInfo.mapProvider = self._options.mapProvider;
  } else {
    self._options.mapProvider = self._options.mapInfo.mapProvider;
  }

  if (_.indexOf(validProviders, self._options.mapProvider) !== -1) {
    // load resources
    loadResource(mapsLibraries[self._options.mapProvider].css, 'css');
    loadResource(mapsLibraries[self._options.mapProvider].js, 'script', mapProvidedLoadedMessage);
  } else {
    throw new Error(self._mapProviderError);
  }
  // event listen
  emitter.eventBus.on(mapProvidedLoadedMessage, function(data) {
    self._nativeMap = mcdbMap.create(selector, self._options.mapInfo, self._options.mapProvider, self._nativeMap);
  });
};

MiniCarto.prototype.createVis = function(selector, vizjson, options) {
  options = options || {};
  this._options = _.extend(this._options, options);
  this._htmlSelector = selector;
  this._vizjson = vizjson;
  this._mapsApiConfig = vizjson['maps_api_config'];

  this.createMap(selector, vizjson);
  this.getCartoData();
};

MiniCarto.prototype.getCartoData = function() {
  var username = this._mapsApiConfig['user_name'];
  var urlAPI = 'https://' + username + '.carto.com/api/v1/map';
  var mcdbCall = new McdbCall();
  mcdbCall.ajaxPOST(urlAPI, mcdbJSonParser.getLayersData(this._vizjson));
};

MiniCarto.prototype.createLayers = function(data) {
  if(this._options.groupLayers === true) {
    this.createGroupLayers(data, this._mapsApiConfig, this._options.mapProvider);
  } else {
    var tileLayers = mcdbMap.createLayers(data, this._mapsApiConfig, this._options.mapProvider);
    this._layers = {'layer0': tileLayers};
    this._nativeMap.addLayer(tileLayers);
  }
};

MiniCarto.prototype.createGroupLayers = function(data) {
  var self = this;
  var tileLayers = mcdbMap.createGroupLayers(data, this._mapsApiConfig, this._options.mapProvider);
  self._layers = tileLayers.layersGroup;
  _.each(tileLayers.layersGroup, function(tileLayer){
    self._nativeMap.addLayer(tileLayer);
  });
  if (tileLayers.hasOwnProperty('control')) {
    self._nativeMap.addControl(tileLayers.control);
  }
};

MiniCarto.prototype.enableLayer = function(index) {
  mcdbMap.setOpacityToLayer(index, 1, this._layers, this._options.mapProvider);
};

MiniCarto.prototype.disableLayer = function(index) {
  mcdbMap.setOpacityToLayer(index, 0, this._layers, this._options.mapProvider);
};

MiniCarto.prototype.setSQL = function(query) {
  mcdbJSonParser.setSQL(query, this._vizjson);
  this.createVis(this._htmlSelector, this._vizjson);
};

// export minicartodb for fun
module.exports = MiniCarto;
