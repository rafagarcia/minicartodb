//  minicartodb.js 1.0.0
//  call tests
//  [url bitbucket]
//  (c) 2017 Pablo Rodríguez, @therealpablors
//  Carto Test Frontend developer 2017
//  minicarto may be freely distributed under the MIT license.

'use strict';

var Call = require('../../src/utils/mcdb-call');
var emitter = require('../../src/utils/mcdb-events');
var emitterMessages = emitter.eventBus._messages;

describe('mcdb-call module', function () {
  var $ = {
    ajax: {}
  };

  var call = new Call();

  describe('constructor', function() {
    var newCall;
    var newCallWithUrl;

    beforeAll(function() {
      newCall = new Call({testing: 'is great'});
      newCallWithUrl = new Call({url: 'fakeUrl'});
    });

    it('should have a _options property', function() {
      expect(newCall.hasOwnProperty('_options')).toBeTruthy();
    });

    it('should set new properties to _options', function() {
      expect(newCall._options.hasOwnProperty('testing')).toBeTruthy();
      expect(newCall._options.testing).toEqual('is great');
    });

    it('should have to setted url into _options', function() {
      expect(newCallWithUrl._options.url).toBe('fakeUrl');
    });
  });

  describe('getCartoData', function() {
    var functionSpy;
    beforeAll(function() {
      $.ajax = jasmine.createSpy('ajaxSpy').and.callFake(function(obj) {
        obj.success('data');
      });
      functionSpy = jasmine.createSpy('emitterSpy');
      emitter.eventBus.on(emitterMessages.getCartoData, functionSpy);
      window.$ = $;
      call.ajaxPOST('url', 'data');
    });

    it('should call $.ajax', function() {
      expect($.ajax).toHaveBeenCalledWith(jasmine.any(Object));
    });

    it('should emit an event when $.ajax is success', function() {
      expect($.ajax).toHaveBeenCalledWith(jasmine.any(Object));
      expect(functionSpy).toHaveBeenCalledWith('data');
    });
  });
});
