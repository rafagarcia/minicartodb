//  minicartodb.js 1.0.0
//  mcdb map test
//  [url bitbucket]
//  (c) 2017 Pablo Rodríguez, @therealpablors
//  Carto Test Frontend developer 2017
//  minicarto may be freely distributed under the MIT license.

'use strict';

var mcdbMap = require('../../src/api/mcdb-map');
var _ = require('underscore');

describe('mcdb module', function () {
  window.L = {
    map: function(selector) {
      return {
        setView: function(center, zoom) {
          return {
            remove: function() {},
            center: center,
            zoom: zoom
          };
        }
      };
    },
    tileLayer: function(url, obj) {
      return {
        url: url,
        data: obj
      };
    },
    control: {
      layers: function(obj) {
        return 'fake-control';
      }
    }
  };

  window.ol = {
    Map: function(obj) {
      return {
        selector: obj.target,
        layers: obj.layers,
        view: obj.view,
        setTarget: function() {}
      };
    },
    layer: {
      Tile: function(obj) {
        return obj.source;
      }
    },
    source: {
      OSM: function() {
        return 'fake-source';
      },
      XYZ: function(data) {
        return data;
      }
    },
    View: function(obj) {
      return {
        center: obj.center,
        zoom: obj.zoom
      };
    },
    proj: {
      fromLonLat: function(center) {
        return center;
      }
    }
  };

  describe('object properties', function() {
    it('should have layergroupid and xyzTemplateURL properties', function() {
      expect(mcdbMap._layergroupid).toEqual(0);
      expect(mcdbMap._xyzTemplateURL).toEqual('http://ashbu.cartocdn.com/{username}/api/v1/map/{layergroupid}/{index}/{z}/{x}/{y}.png');
    });
  });

  describe('createMap', function() {
    var selector = 'fake';
    var mapInfo = {
      remove: function(){},
      center: 'fake-center',
      zoom: 'fake-zoom'
    };
    var createMapResultLeaflet = {};
    var rebuildMapResultLeaflet = {};
    var createMapResultOpenLayers = {};
    var rebuildMapResultOpenLayers = {};

    var openlayersMapObject = {
      selector: selector,
      layers: [{
      }],
      view: {
        center: mapInfo.center,
        zoom: mapInfo.zoom
      }};

    beforeAll(function() {
      createMapResultLeaflet = mcdbMap.create(selector, mapInfo, 'leaflet');
      createMapResultOpenLayers = mcdbMap.create(selector, mapInfo, 'openlayers');
      mapInfo.center = 'new-fake-center';
      mapInfo.zoom = 'new-fake-zoom';
      rebuildMapResultLeaflet = mcdbMap.create(selector, mapInfo, 'leaflet', createMapResultLeaflet);
      rebuildMapResultOpenLayers = mcdbMap.create(selector, mapInfo, 'openlayers', createMapResultOpenLayers);
    });

    it('call L.map and returns a map object - leaflet', function() {
      expect(createMapResultLeaflet.center).toEqual('fake-center');
      expect(createMapResultLeaflet.zoom).toEqual('fake-zoom');
    });

    it('call ol.Map and returns a map object - openlayers', function() {
      expect(createMapResultOpenLayers.selector).toEqual(selector);
      expect(createMapResultOpenLayers.view.center).toEqual('fake-center');
      expect(createMapResultOpenLayers.view.zoom).toEqual('fake-zoom');
    });

    it('should delete exist map and create a new one with new data - leaflet', function() {
      expect(rebuildMapResultLeaflet.center).toEqual('new-fake-center');
      expect(rebuildMapResultLeaflet.zoom).toEqual('new-fake-zoom');
    });

    it('should delete exist map and create a new one with new data - leaflet', function() {
      expect(rebuildMapResultOpenLayers.selector).toEqual(selector);
      expect(rebuildMapResultOpenLayers.view.center).toEqual('new-fake-center');
      expect(rebuildMapResultOpenLayers.view.zoom).toEqual('new-fake-zoom');
    });
  });

  describe('createLayers', function() {
    var createLayersLeaflet = {};
    var createLayersOpenLayers = {};
    var data = {
      layergroupid: 'fake-layergroupid',
      metadata: {
        layers: [
          {type: 'fake'},
          {type: 'fake'},
          {type: 'fake'}
        ]
      }
    };
    var config = {
      'user_name': 'fakeUsername',
      maxZoom: 'fake-maxZoom',
      minZoom: 'fake-minZoom'
    };

    beforeAll(function() {
      createLayersLeaflet = mcdbMap.createLayers(data, config, 'leaflet');
      createLayersOpenLayers = mcdbMap.createLayers(data, config, 'openlayers');
    });

    it('should returns layer - leaftlet', function() {
      expect(createLayersLeaflet.url).toEqual(mcdbMap._xyzTemplateURL);
      expect(createLayersLeaflet.data).toEqual({
        'username': 'fakeUsername',
        'layergroupid': 'fake-layergroupid',
        maxZoom: 'fake-maxZoom',
        minZoom: 'fake-minZoom',
        index: [0,1,2]
      });
    });

    it('should returns layer - openlayers', function() {
      expect(createLayersOpenLayers.url).toEqual('http://ashbu.cartocdn.com/' + config['user_name'] + '/api/v1/map/' + data.layergroupid + '/0,1,2/{z}/{x}/{y}.png');
    });
  });

  describe('createGroupLayers', function() {
    var data = {
      layergroupid: 'fake-layergroupid',
      metadata: {
        layers: [
          {type: 'fake'},
          {type: 'fake'},
          {type: 'fake'}
        ]
      }
    };
    var config = {
      'user_name': 'fakeUsername',
      maxZoom: 'fake-maxZoom',
      minZoom: 'fake-minZoom'
    };

    var createGroupLayersLeaflet;
    var createGroupLayersOpenLayers;

    beforeAll(function() {
      createGroupLayersLeaflet = mcdbMap.createGroupLayers(data, config, 'leaflet');
      createGroupLayersOpenLayers = mcdbMap.createGroupLayers(data, config, 'openlayers');
    });

    it('should create group layers for leaflet', function() {
      expect(createGroupLayersLeaflet.hasOwnProperty('layersGroup')).toBeTruthy();
      expect(createGroupLayersLeaflet.hasOwnProperty('baseLayer')).toBeTruthy();
      expect(createGroupLayersLeaflet.control).toEqual('fake-control');
    });

    it('should create group layers for openlayers', function() {
      expect(createGroupLayersOpenLayers.hasOwnProperty('layersGroup')).toBeTruthy();
      expect(createGroupLayersOpenLayers.hasOwnProperty('baseLayer')).toBeTruthy();
    });
  });

  describe('setOpacityToLayer', function() {

    var setOpacityLeafLet;
    var setOpacityOpenLayers;
    var layers = [
      {
        opacity: 0,
        setOpacity: function(opacity) {
          this.opacity = opacity;
          return this.opacity;
        }
      }
    ];

    beforeAll(function() {
      setOpacityLeafLet = mcdbMap.setOpacityToLayer(0, 1, layers, 'leaflet');
      setOpacityOpenLayers = mcdbMap.setOpacityToLayer(0, 1, layers, 'leaflet');
    });

    it('should change opacity to layer - leaftlet',function() {
      expect(setOpacityLeafLet).toEqual(1);
    });
    it('should change opacity to layer - openlayers',function() {
      expect(setOpacityOpenLayers).toEqual(1);
    });
  });
});
